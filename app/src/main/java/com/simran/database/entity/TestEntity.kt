package com.simran.database.entity


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "Test")
data class TestEntity(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "name") @PrimaryKey val name: String,
    @ColumnInfo(name = "time") @PrimaryKey val email: Long,
    @ColumnInfo(name = "firstQuestion") @PrimaryKey val firstQuestion: String,
    @ColumnInfo(name = "firstAnswer") @PrimaryKey val firstAnswer: Long,
    @ColumnInfo(name = "secondQuestion") @PrimaryKey val secondQuestion: String,
    @ColumnInfo(name = "secondAnswer") @PrimaryKey val secondAnswer: Long
)