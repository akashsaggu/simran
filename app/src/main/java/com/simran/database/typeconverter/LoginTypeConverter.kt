package com.simran.database.typeconverter

import com.simran.database.entity.LoginType
import com.simran.database.typeconverter.base.ObjectConverter


object LoginTypeConverter : ObjectConverter<LoginType>()