package com.simran.database.typeconverter.base

import androidx.room.TypeConverter
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

abstract class ObjectConverter<T> {


    @TypeConverter

    fun convertToBytes(`object`: T): ByteArray {
        ByteArrayOutputStream().use { bos ->
            ObjectOutputStream(bos).use { out ->
                out.writeObject(`object`)
                return bos.toByteArray()
            }
        }
    }

    @TypeConverter
    fun convertFromBytes(bytes: ByteArray): T {
        ByteArrayInputStream(bytes).use { bis ->
            ObjectInputStream(bis).use { `in` -> return `in`.readObject() as T }
        }
    }
}