package com.simran.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.simran.app.App
import com.simran.database.dao.TestDao
import com.simran.database.entity.TestEntity
import com.simran.database.typeconverter.LoginTypeConverter

@Database(entities = [TestEntity::class], version = 1)
@TypeConverters(value = [LoginTypeConverter::class])
abstract class TestDatabase : RoomDatabase() {

    abstract fun testDao(): TestDao


    companion object {
        private var INSTANCE: TestDatabase? = null
        fun getInstance(): TestDatabase? {
            if (INSTANCE == null) {
                synchronized(TestDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        App.getApp(),
                        TestDatabase::class.java, "ruth.db"
                    )
                        //.allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}