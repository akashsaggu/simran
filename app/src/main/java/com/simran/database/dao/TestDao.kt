package com.simran.database.dao


import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.simran.database.entity.TestEntity


@Dao
interface TestDao : BaseDao<TestEntity> {

    @Query("SELECT * FROM Test")
    fun getAllTests(): LiveData<List<TestEntity>>

}