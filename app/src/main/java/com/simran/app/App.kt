package com.simran.app

import android.app.Application
import com.simran.di.common
import com.simran.di.viewModels
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        application = this
        startKoin {
            androidContext(this@App)
            modules(listOf(viewModels, common))
        }
    }

    companion object {
        lateinit var application: App
        @JvmStatic
        fun getApp() = application
    }

}
