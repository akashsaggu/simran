package com.simran.di

import com.simran.base.BaseViewModel
import com.simran.repository.LocalRepository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModels = module {
    viewModel { BaseViewModel() }
}

val common = module {
    single { LocalRepository() }
}