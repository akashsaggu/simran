package com.simran.ui.viewmodel

import androidx.lifecycle.LiveData
import com.simran.base.BaseViewModel
import com.simran.database.entity.TestEntity

class TestViewmodel : BaseViewModel() {

    private val testLd by lazy { getLocalRepository().getAllTests() }


    fun getAllTests(): LiveData<List<TestEntity>>? {
        return testLd
    }

}