package com.simran.base

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

open class BaseActivity(@IdRes private val container: Int = android.R.id.content) :
    AppCompatActivity() {
    private val supportFm by lazy { supportFragmentManager }


    fun addFragment(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFm.beginTransaction()
        transaction.add(container, fragment, fragment::class.java.simpleName)
        transaction.addToBackStack(if (addToBackStack) "Added" else null)
        transaction.commitAllowingStateLoss()
    }

    fun replaceFragment(fragment: Fragment, addToBackStack: Boolean) {
        val transaction = supportFm.beginTransaction()
        transaction.replace(container, fragment, fragment::class.java.simpleName)
        transaction.addToBackStack(if (addToBackStack) "Added" else null)
        transaction.commitAllowingStateLoss()
    }

    open fun containsOnlyFragment(): Boolean {
        return true
    }

    override fun onBackPressed() {
        if (containsOnlyFragment()) {
            if (supportFm.backStackEntryCount == 1) {
                finish()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}